gulp = require 'gulp'
gutil = require 'gulp-util'
nib = require 'nib'
stylus = require 'gulp-stylus'
coffee = require 'gulp-coffee'
uglify = require 'gulp-uglify'
concat = require 'gulp-concat'
browserify = require 'browserify'
through2 = require 'through2'
yargs = require('yargs').argv
rename = require 'gulp-rename'
cjsx = require 'gulp-cjsx'

handleError = (e, cb) ->
  gutil.log gutil.colors.red('Error'), e
  @emit 'end'
  #cb?()

paths =
  coffee:
    src: './src/coffee/**/*.coffee'
    dist: './src/js'
  cjsx:
    src: './src/cjsx/**/*.cjsx'
    dist: './src/js'
  styl:
    src: './src/styl/**/*.styl'
    srcMain: './src/styl/main.styl'
    dist: './src/css'
  css:
    dist: './src/dist'
  js:
    src: ['./src/js/**/*.js']
    dest: './src/js/app.js'
  dist: './src/dist'

gulp.task 'browserify', ->
  options =
    insertGlobals : false
    debug: !yargs.production
    standalone: 'MapAppMeasurement'

  gulp.src(paths.js.dest)
    .pipe through2.obj (file, enc, next) ->
      browserify(file.path, options)
        .transform 'browserify-shim'
        .external ['React', 'openlayers']

        .bundle (err, res) ->
          #assumes file.contents is a Buffer
          file.contents = res
          next null, file

    .pipe(gulp.dest(paths.dist))

# zatim upraveno pro example
gulp.task 'coffee', ->
  gulp.src(paths.coffee.src)
    .pipe(coffee({bare: true}).on('error', handleError))
    .pipe(gulp.dest(paths.coffee.dist))

# zatim upraveno pro example
gulp.task 'compileJs', ['coffee'], ->
  gulp.src('./src/dist/app.js')
    #.pipe(concat('app.min.js'))
    .pipe(uglify().on('error', handleError))
    .pipe(rename({extname: '.min.js'}))
    .pipe(gulp.dest(paths.dist))

gulp.task 'cjsx', ->
  gulp.src(paths.cjsx.src)
    .pipe(cjsx({bare: true}).on('error', handleError))
    .pipe(gulp.dest(paths.cjsx.dist))

gulp.task 'stylus', ->
  gulp.src(paths.styl.srcMain)
    .pipe(stylus({compress: true, use: [nib()]}).on('error', handleError))
    .pipe(rename("app.min.css"))
    .pipe(gulp.dest(paths.css.dist))

gulp.task 'watch', ->
  gulp.watch paths.cjsx.src, ['cjsx', 'compileJs', 'browserify']
  gulp.watch paths.coffee.src, ['coffee', 'compileJs', 'browserify']
  gulp.watch paths.styl.src, -> gulp.start 'stylus'

gulp.task 'default', ['coffee', 'cjsx', 'browserify', 'compileJs', 'stylus'], ->
