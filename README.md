# Map length measurement

App for displaying maps with measurement tool.

## Getting Started

To use this app:

* Download the latest release on [Bitbucket](https://bitbucket.org/milan_hrubes/map-length-measurement)
* run bower install

For developing you have to run command **npm install** (_install the devel packages_)

## Creator

This app was created by and is maintained by **Milan Hrubeš**.

* https://twitter.com/mildahrubes
* https://bitbucket.org/milan_hrubes/

## Copyright and License

Copyright 2015 Milan Hrubeš released under the MIT license.
