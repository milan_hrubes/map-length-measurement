var React, appMapPanel;

React = require('react');

appMapPanel = React.createClass({
  displayName: 'PanelComponent',
  getInitialState: function() {
    return {
      lengths: [],
      showLengths: false,
      resultLength: ''
    };
  },
  createRows: function() {
    return this.state.lengths.map(function(item, i) {
      return React.createElement("div", {
        "className": 'row'
      }, React.createElement("span", null, i + 1, ".", item));
    });
  },
  createHeaderRow: function() {
    return React.createElement("div", {
      "className": 'row-header'
    }, React.createElement("span", null, "Úsek"), React.createElement("span", null, "Vzdálenost"));
  },
  createResultRow: function() {
    return React.createElement("div", {
      "className": 'resultLength'
    }, React.createElement("span", null, "Celková délka trasy"), React.createElement("p", null, " ", React.createElement("strong", null, this.state.resultLength), " "));
  },
  createNotice: function() {
    var style;
    style = {
      color: '#ccc',
      fontSize: '22px',
      textAlign: 'center'
    };
    return React.createElement("p", {
      "style": style
    }, "Zadejte alespoň dva body");
  },
  render: function() {
    var headerRows, notice, resultLength, rowsOfLengths;
    rowsOfLengths = this.createRows();
    headerRows = this.state.lengths.length !== 0 ? this.createHeaderRow() : void 0;
    resultLength = this.state.resultLength ? this.createResultRow() : void 0;
    notice = this.state.lengths.length === 0 ? this.createNotice() : null;
    return React.createElement("div", null, React.createElement("h2", null, "Měření délek"), notice, headerRows, rowsOfLengths, resultLength);
  }
});

if (typeof module !== "undefined" && module !== null) {
  module.exports = appMapPanel;
}
