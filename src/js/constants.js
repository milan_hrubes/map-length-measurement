var constants;

constants = {
  HELPMSG: 'Klikněte pro zadání bodu',
  HELPMSGNEXTPOINT: 'Klikněte pro zadání bodu nebo dvojklikem ukončete',
  WGS84SPHERE: 6378137,
  style: {
    drawTool: {
      FILLCOLOR: 'rgba(255, 255, 255, 0.2)',
      STROKECOLOR: 'rgba(83, 194, 207, 0.8)',
      LINEDASH: [10, 10],
      WIDTH: 4,
      RADIUS: 5
    },
    vectorLayer: {
      FILLCOLOR: 'rgba(83, 194, 207, 0.2)',
      RADIUS: 7,
      STROKECOLOR: 'rgba(255, 255, 255, 0.5)',
      WIDTH: 5,
      STROKECOLORSECOND: 'rgba(83, 194, 207, 0.7)',
      WIDTHSECOND: 3
    }
  }
};

if (typeof module !== "undefined" && module !== null) {
  module.exports = constants;
}
