var Ol3, PanelComponent, React, VectorLayer, appMap;

PanelComponent = require('./panelComponent.js');

VectorLayer = require('./vectorLayer.js');

React = require('react');

Ol3 = require('openlayers');

appMap = {

  /**
     * Rendered map
     * @type {ol.Map}
   */
  renderedMap: null,

  /**
     * Rendered layer for vector data
     * @type {Object}
   */
  vectorLayer: null,

  /**
     * React component of panel
     * @type {Class}
   */
  panelComponent: null,

  /*
    * Initialization of map
    *
    * @param {String} target - id of DOM element for map
   */
  init: function(target) {
    this.renderedMap = new Ol3.Map({
      controls: this.setControls(),
      layers: [],
      view: this.setMapView(),
      target: target
    });
    this.setLayers();
    this.createLayerSwitcher();
    return this.panelComponent = React.render(React.createElement(PanelComponent, null), document.getElementById('panel'));
  },

  /*
    * Set inital map view (initial zoom, projection, ..)
    *
    * @return {ol.View}
   */
  setMapView: function() {
    return new Ol3.View({
      center: ol.proj.transform([15, 50], 'EPSG:4326', 'EPSG:3857'),
      zoom: 7,
      projection: 'EPSG:3857'
    });
  },

  /*
    * Set layers of map
    * create layers {ol.layer} and set parameters
   */
  setLayers: function() {
    var baseGroup, osmLayer, overlaysGroup, satelliteLayer;
    osmLayer = new Ol3.layer.Tile({
      title: 'OSM',
      type: 'base',
      visible: true,
      source: new Ol3.source.OSM()
    });
    satelliteLayer = new Ol3.layer.Tile({
      title: 'Satellite',
      type: 'base',
      visible: false,
      source: new Ol3.source.MapQuest({
        layer: 'sat'
      })
    });
    this.vectorLayer = VectorLayer.createLayer(this.renderedMap);
    baseGroup = this.createLayersGroup('Mapy', [osmLayer, satelliteLayer]);
    overlaysGroup = this.createLayersGroup('Overlays', [this.vectorLayer]);
    this.renderedMap.addLayer(baseGroup);
    return this.renderedMap.addLayer(overlaysGroup);
  },

  /*
    * Create group of layers for layer switcher
    *
    * @param {String} groupTitle - title of the group
    * @param {Array} groupLayers - array of created layers
    * @return {ol.layer.Group}
   */
  createLayersGroup: function(groupTitle, groupLayers) {
    return new Ol3.layer.Group({
      title: groupTitle,
      layers: groupLayers
    });
  },

  /*
    * Set controls of the map
    *
    * @return {ol.control.defaults}
   */
  setControls: function() {
    return Ol3.control.defaults().extend([
      new Ol3.control.ScaleLine({
        units: 'metric'
      }), new Ol3.control.ZoomSlider()
    ]);
  },

  /*
    * Create layer switcher to switch layers
   */
  createLayerSwitcher: function() {
    var layerSwitcher;
    layerSwitcher = new Ol3.control.LayerSwitcher({
      tipLabel: 'Legenda'
    });
    return this.renderedMap.addControl(layerSwitcher);
  }
};

if (typeof module !== "undefined" && module !== null) {
  module.exports = appMap;
}
