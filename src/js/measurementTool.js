var Constants, Ol3, measurementTool;

Ol3 = require('openlayers');

Constants = require('./constants.js');

measurementTool = {

  /**
     * Actual rendering route
     * @type {ol.Feature}
   */
  featureMeasure: null,

  /**
     * DOM element for a label with total measured length
     * @type {Element}
   */
  measureTooltipElement: null,

  /**
     * Layer to display a label with total measured length
     * @type {ol.Overlay}
   */
  measureTooltip: null,

  /**
     * DOM element for a label on cursor Help
     * @type {Element}
   */
  helpTooltipElement: null,

  /**
     * Layer to display a label with Help
     * @type {ol.Overlay}
   */
  helpTooltip: null,

  /**
     * Tool for drawing lines
     * @type {ol.interaction.Draw}
   */
  drawTool: null,
  init: function(renderedMap, vectorLayer) {
    this.renderedMap = renderedMap;
    this.vectorLayer = vectorLayer;
    if (!this.drawTool) {
      this.renderedMap.on('pointermove', this.pointerMoveHandler);
      return this.addInteraction();
    }
  },
  pointerMoveHandler: function(ev) {
    var geom, output, toolTipMsg, tooltipCoord;
    if (ev.dragging) {
      return;
    }
    tooltipCoord = ev.coordinate;
    toolTipMsg = Constants.HELPMSG;
    if (measurementTool.featureMeasure) {
      output = void 0;
      geom = measurementTool.featureMeasure.getGeometry();
      if (geom instanceof Ol3.geom.LineString) {
        output = measurementTool.formatLengthLineString(geom);
        tooltipCoord = geom.getLastCoordinate();
        toolTipMsg = Constants.HELPMSGNEXTPOINT;
      }
      measurementTool.measureTooltipElement.innerHTML = output;
      measurementTool.measureTooltip.setPosition(tooltipCoord);
    }
    measurementTool.helpTooltipElement.innerHTML = toolTipMsg;
    return measurementTool.helpTooltip.setPosition(ev.coordinate);
  },
  addInteraction: function() {
    this.drawTool = new Ol3.interaction.Draw({
      source: this.vectorLayer.getSource(),
      type: 'LineString',
      style: this.createStyle()
    });
    this.renderedMap.addInteraction(this.drawTool);
    this.createMeasureTooltip();
    this.createHelpTooltip();
    this.renderedMap.on('singleclick', function(ev) {
      return measurementTool.addedSegment('drawContinuing');
    });
    this.drawTool.on('drawstart', this.drawStart);
    return this.drawTool.on('drawend', this.drawEnd);
  },
  createStyle: function() {
    return new Ol3.style.Style({
      fill: new Ol3.style.Fill({
        color: Constants.style.drawTool.FILLCOLOR
      }),
      stroke: new Ol3.style.Stroke({
        color: Constants.style.drawTool.STROKECOLOR,
        lineDash: Constants.style.drawTool.LINEDASH,
        width: Constants.style.drawTool.WIDTH
      }),
      image: new Ol3.style.Circle({
        radius: Constants.style.drawTool.RADIUS,
        stroke: new Ol3.style.Stroke({
          color: Constants.style.drawTool.STROKECOLOR
        }),
        fill: new Ol3.style.Fill({
          color: Constants.style.drawTool.FILLCOLOR
        })
      })
    });
  },
  addedSegment: function(type) {
    var coordinates, index, length, lengths, ref;
    index = 0;
    lengths = [];
    coordinates = (ref = measurementTool.featureMeasure) != null ? ref.getGeometry().getCoordinates() : void 0;
    if (type === 'drawContinuing') {
      if (coordinates != null) {
        coordinates.pop();
      }
    }
    while (index < (coordinates != null ? coordinates.length : void 0)) {
      if (coordinates[index + 1]) {
        length = this.formatLengthTransform(coordinates[index], coordinates[index + 1]);
        if (length !== 0) {
          lengths.push(this.formatCompleteLength(length));
        }
      }
      ++index;
    }
    return MapAppMeasurement.panelComponent.setState({
      lengths: lengths
    });
  },
  drawStart: function(ev) {
    measurementTool.featureMeasure = ev.feature;
    return measurementTool.clearPreviousMeasure();
  },
  drawEnd: function(ev) {
    var resultLengthValue;
    measurementTool.addedSegment('drawEnd');
    resultLengthValue = measurementTool.measureTooltipElement.innerHTML;
    MapAppMeasurement.panelComponent.setState({
      resultLength: resultLengthValue
    });
    measurementTool.measureTooltipElement.id = 'result-tooltip';
    measurementTool.measureTooltipElement.className = 'tooltip tooltip-static';
    measurementTool.measureTooltip.setOffset([0, -7]);
    measurementTool.featureMeasure = null;
    measurementTool.measureTooltipElement = null;
    return measurementTool.createMeasureTooltip();
  },
  clearPreviousMeasure: function() {
    var overlays;
    overlays = this.renderedMap.getOverlays();
    overlays.forEach(function(overlay) {
      if (overlay.getElement().id === 'result-tooltip') {
        return overlays.remove(overlay);
      }
    });
    this.vectorLayer.getSource().clear();
    return MapAppMeasurement.panelComponent.setState({
      resultLength: null,
      lengths: []
    });
  },
  createHelpTooltip: function() {
    if (this.helpTooltipElement) {
      this.helpTooltipElement.parentNode.removeChild(this.helpTooltipElement);
    }
    this.helpTooltipElement = this.createDivElement('tooltip');
    this.helpTooltip = this.createOverlay(this.helpTooltipElement, [15, 0], 'center-left');
    return this.renderedMap.addOverlay(this.helpTooltip);
  },
  createMeasureTooltip: function() {
    if (this.measureTooltipElement) {
      this.measureTooltipElement.parentNode.removeChild(this.measureTooltipElement);
    }
    this.measureTooltipElement = this.createDivElement('tooltip tooltip-measure');
    this.measureTooltip = this.createOverlay(this.measureTooltipElement, [0, -15], 'bottom-center');
    return this.renderedMap.addOverlay(this.measureTooltip);
  },
  createDivElement: function(className) {
    var element;
    element = document.createElement('div');
    element.className = className;
    return element;
  },
  createOverlay: function(element, offset, position) {
    return new Ol3.Overlay({
      element: element,
      offset: offset,
      positioning: position
    });
  },
  formatLengthLineString: function(line) {
    var coordinates, index, len, length, sourceProj;
    coordinates = line.getCoordinates();
    sourceProj = this.renderedMap.getView().getProjection();
    length = 0;
    index = 0;
    len = coordinates.length - 1;
    while (index < len) {
      length += this.formatLengthTransform(coordinates[index], coordinates[index + 1]);
      ++index;
    }
    return this.formatCompleteLength(length);
  },
  formatLengthTransform: function(coord1, coord2) {
    var c1, c2, sourceProj, wgs84Sphere;
    wgs84Sphere = new Ol3.Sphere(Constants.WGS84SPHERE);
    sourceProj = this.renderedMap.getView().getProjection();
    c1 = Ol3.proj.transform(coord1, sourceProj, 'EPSG:4326');
    c2 = Ol3.proj.transform(coord2, sourceProj, 'EPSG:4326');
    return wgs84Sphere.haversineDistance(c1, c2);
  },
  formatCompleteLength: function(length) {
    var formatedLength;
    if (length > 100) {
      formatedLength = Math.round(length / 1000 * 100) / 100 + ' ' + 'km';
    } else {
      formatedLength = Math.round(length * 100) / 100 + ' ' + 'm';
    }
    return formatedLength;
  }
};

if (typeof module !== "undefined" && module !== null) {
  module.exports = measurementTool;
}
