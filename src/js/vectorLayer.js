var Constants, MeasurementTool, Ol3, vectorLayer;

Ol3 = require('openlayers');

MeasurementTool = require('./measurementTool.js');

Constants = require('./constants.js');

vectorLayer = {

  /**
     * Rendered vector layer
     * @type {ol.layer.Vector}
   */
  layer: null,

  /**
     * Rendered map
     * @type {ol.Map}
   */
  renderedMap: null,

  /*
    * Initialization of the layer
    *
    * @param {ol.Map} renderedMap - rendered ol3 map
    * @return {ol.layer.Vector}
   */
  createLayer: function(renderedMap) {
    this.renderedMap = renderedMap;
    this.layer = new Ol3.layer.Vector({
      title: 'Vector',
      source: new Ol3.source.Vector(),
      style: this.createStyles()
    });
    MeasurementTool.init(this.renderedMap, this.layer);
    return this.layer;
  },

  /*
    * Create styles of vector layer
    *
    * @return {Array}
   */
  createStyles: function() {
    return [
      new Ol3.style.Style({
        fill: new Ol3.style.Fill({
          color: Constants.style.vectorLayer.FILLCOLOR
        }),
        stroke: new Ol3.style.Stroke({
          color: Constants.style.vectorLayer.STROKECOLOR,
          width: Constants.style.vectorLayer.WIDTH
        }),
        image: new Ol3.style.Circle({
          radius: Constants.style.vectorLayer.RADIUS,
          fill: new Ol3.style.Fill({
            color: Constants.style.vectorLayer.FILLCOLOR
          })
        })
      }), new Ol3.style.Style({
        stroke: new Ol3.style.Stroke({
          color: Constants.style.vectorLayer.STROKECOLORSECOND,
          width: Constants.style.vectorLayer.WIDTHSECOND
        })
      })
    ];
  }
};

if (typeof module !== "undefined" && module !== null) {
  module.exports = vectorLayer;
}
