PanelComponent = require './panelComponent.js'
VectorLayer = require './vectorLayer.js'
React = require 'react'
Ol3 = require 'openlayers'

appMap =

  ###*
     * Rendered map
     * @type {ol.Map}
  ###
  renderedMap: null

  ###*
     * Rendered layer for vector data
     * @type {Object}
  ###
  vectorLayer: null

  ###*
     * React component of panel
     * @type {Class}
  ###
  panelComponent: null

  ###
    * Initialization of map
    *
    * @param {String} target - id of DOM element for map
  ###
  init: (target) ->
    @renderedMap = new Ol3.Map({
      controls: @setControls(),
      layers: [],
      view: @setMapView()
      target: target
    })

    @setLayers()
    @createLayerSwitcher()

    @panelComponent = React.render(
      React.createElement(PanelComponent, null),
      document.getElementById('panel')
    )

  ###
    * Set inital map view (initial zoom, projection, ..)
    *
    * @return {ol.View}
  ###
  setMapView: ->
    return new Ol3.View({
      center: ol.proj.transform([15, 50], 'EPSG:4326', 'EPSG:3857')
      zoom: 7
      projection: 'EPSG:3857'
    })

  ###
    * Set layers of map
    * create layers {ol.layer} and set parameters
  ###
  setLayers: ->
    osmLayer = new Ol3.layer.Tile({
      title: 'OSM'
      type: 'base'
      visible: true
      source: new Ol3.source.OSM()
    })

    satelliteLayer = new Ol3.layer.Tile({
      title: 'Satellite'
      type: 'base'
      visible: false
      source: new Ol3.source.MapQuest({layer: 'sat'})
    })

    @vectorLayer = VectorLayer.createLayer @renderedMap

    baseGroup = @createLayersGroup 'Mapy', [osmLayer, satelliteLayer]
    overlaysGroup = @createLayersGroup 'Overlays', [@vectorLayer]

    @renderedMap.addLayer baseGroup
    @renderedMap.addLayer overlaysGroup

  ###
    * Create group of layers for layer switcher
    *
    * @param {String} groupTitle - title of the group
    * @param {Array} groupLayers - array of created layers
    * @return {ol.layer.Group}
  ###
  createLayersGroup: (groupTitle, groupLayers) ->
    return new Ol3.layer.Group({
      title: groupTitle
      layers: groupLayers
    })

  ###
    * Set controls of the map
    *
    * @return {ol.control.defaults}
  ###
  setControls: ->
    return Ol3.control.defaults().extend([
      new Ol3.control.ScaleLine({
        units: 'metric' #degrees
      }),
      new Ol3.control.ZoomSlider()
      #new Ol3.control.FullScreen(),
      #new Ol3.control.MousePosition()
    ])

  ###
    * Create layer switcher to switch layers
  ###
  createLayerSwitcher: ->
    layerSwitcher = new Ol3.control.LayerSwitcher({tipLabel: 'Legenda'})
    @renderedMap.addControl layerSwitcher

module?.exports = appMap
