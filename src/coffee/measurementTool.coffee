Ol3 = require 'openlayers'
Constants = require './constants.js'

measurementTool =

  ###*
     * Actual rendering route
     * @type {ol.Feature}
  ###
  featureMeasure: null

  ###*
     * DOM element for a label with total measured length
     * @type {Element}
  ###
  measureTooltipElement: null

  ###*
     * Layer to display a label with total measured length
     * @type {ol.Overlay}
  ###
  measureTooltip: null

  ###*
     * DOM element for a label on cursor Help
     * @type {Element}
  ###
  helpTooltipElement: null

  ###*
     * Layer to display a label with Help
     * @type {ol.Overlay}
  ###
  helpTooltip: null

  ###*
     * Tool for drawing lines
     * @type {ol.interaction.Draw}
  ###
  drawTool: null

  init: (@renderedMap, @vectorLayer) ->
    if not @drawTool
      @renderedMap.on 'pointermove', @pointerMoveHandler
      @addInteraction()

  pointerMoveHandler: (ev) ->
    #_this.layer.getVisible()
    if ev.dragging
      return

    tooltipCoord = ev.coordinate
    toolTipMsg = Constants.HELPMSG

    if measurementTool.featureMeasure
      output = undefined
      geom = measurementTool.featureMeasure.getGeometry()

      if geom instanceof Ol3.geom.LineString
        output = measurementTool.formatLengthLineString geom
        tooltipCoord = geom.getLastCoordinate()
        toolTipMsg = Constants.HELPMSGNEXTPOINT

      measurementTool.measureTooltipElement.innerHTML = output
      measurementTool.measureTooltip.setPosition tooltipCoord

    measurementTool.helpTooltipElement.innerHTML = toolTipMsg
    measurementTool.helpTooltip.setPosition ev.coordinate

  addInteraction: ->
    @drawTool = new Ol3.interaction.Draw
      source: @vectorLayer.getSource()
      type: 'LineString' #ol.geom.GeometryType.LINE_STRING
      style: @createStyle()

    @renderedMap.addInteraction @drawTool
    @createMeasureTooltip()
    @createHelpTooltip()

    @renderedMap.on 'singleclick', (ev) ->
      measurementTool.addedSegment 'drawContinuing'

    @drawTool.on 'drawstart', @drawStart
    @drawTool.on 'drawend', @drawEnd

  createStyle: ->
    return new Ol3.style.Style
      fill: new (Ol3.style.Fill)(color: Constants.style.drawTool.FILLCOLOR)

      stroke: new Ol3.style.Stroke(
        color: Constants.style.drawTool.STROKECOLOR
        lineDash: Constants.style.drawTool.LINEDASH
        width: Constants.style.drawTool.WIDTH
      )

      image: new Ol3.style.Circle(
        radius: Constants.style.drawTool.RADIUS
        stroke: new Ol3.style.Stroke(color: Constants.style.drawTool.STROKECOLOR)
        fill: new Ol3.style.Fill(color: Constants.style.drawTool.FILLCOLOR)
      )

  addedSegment: (type) ->
    index = 0
    lengths = []

    coordinates = measurementTool.featureMeasure?.getGeometry().getCoordinates()
    if type is 'drawContinuing' then coordinates?.pop()

    while index < coordinates?.length
      if coordinates[index + 1]
        length = @formatLengthTransform coordinates[index], coordinates[index + 1]

        if length isnt 0 then lengths.push  @formatCompleteLength length

      ++index

    MapAppMeasurement.panelComponent.setState {lengths: lengths}

  drawStart: (ev) ->
    measurementTool.featureMeasure = ev.feature
    measurementTool.clearPreviousMeasure()

  drawEnd: (ev) ->
    measurementTool.addedSegment 'drawEnd' #()
    resultLengthValue = measurementTool.measureTooltipElement.innerHTML
    MapAppMeasurement.panelComponent.setState {resultLength: resultLengthValue}

    measurementTool.measureTooltipElement.id = 'result-tooltip'
    measurementTool.measureTooltipElement.className = 'tooltip tooltip-static'
    measurementTool.measureTooltip.setOffset [0, -7]

    measurementTool.featureMeasure = null
    measurementTool.measureTooltipElement = null
    measurementTool.createMeasureTooltip()

  clearPreviousMeasure: ->
    overlays = @renderedMap.getOverlays()
    overlays.forEach (overlay) ->
      if overlay.getElement().id is 'result-tooltip'
        overlays.remove overlay

    @vectorLayer.getSource().clear()
    MapAppMeasurement.panelComponent.setState {resultLength: null, lengths: []}

  createHelpTooltip: ->
    if @helpTooltipElement
      @helpTooltipElement.parentNode.removeChild @helpTooltipElement

    @helpTooltipElement = @createDivElement 'tooltip'
    @helpTooltip = @createOverlay @helpTooltipElement, [15, 0], 'center-left'

    @renderedMap.addOverlay @helpTooltip

  createMeasureTooltip: ->
    if @measureTooltipElement
      @measureTooltipElement.parentNode.removeChild @measureTooltipElement

    @measureTooltipElement = @createDivElement 'tooltip tooltip-measure'
    @measureTooltip = @createOverlay @measureTooltipElement, [0, -15], 'bottom-center'

    @renderedMap.addOverlay @measureTooltip

  createDivElement: (className) ->
    element = document.createElement 'div'
    element.className = className

    return element

  createOverlay: (element, offset, position)->
    new Ol3.Overlay({
      element: element
      offset: offset
      positioning: position
    })

  formatLengthLineString: (line) ->
    coordinates = line.getCoordinates()
    sourceProj = @renderedMap.getView().getProjection()

    length = 0
    index = 0
    len = coordinates.length - 1

    while index < len
      length += @formatLengthTransform coordinates[index], coordinates[index + 1]
      ++index

    return @formatCompleteLength length

  formatLengthTransform: (coord1, coord2) ->
    wgs84Sphere = new Ol3.Sphere Constants.WGS84SPHERE
    sourceProj = @renderedMap.getView().getProjection()

    c1 = Ol3.proj.transform(coord1, sourceProj, 'EPSG:4326')
    c2 = Ol3.proj.transform(coord2, sourceProj, 'EPSG:4326')

    return wgs84Sphere.haversineDistance c1, c2

  formatCompleteLength: (length) ->
    if length > 100
      formatedLength = Math.round(length / 1000 * 100) / 100 + ' ' + 'km'
    else
      formatedLength = Math.round(length * 100) / 100 + ' ' + 'm'

    return formatedLength


module?.exports = measurementTool
