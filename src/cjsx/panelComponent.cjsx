React = require 'react'

appMapPanel = React.createClass
  displayName: 'PanelComponent'

  getInitialState: ->
    lengths: []
    showLengths: false
    resultLength: ''

  createRows: ->
    @state.lengths.map (item, i) ->
      <div className='row'>
        <span>{i+1}.{item}</span>
      </div>

  createHeaderRow: ->
    <div className='row-header'>
      <span>Úsek</span>
      <span>Vzdálenost</span>
    </div>

  createResultRow: ->
    <div className='resultLength'>
      <span>Celková délka trasy</span>
      <p> <strong>{@state.resultLength}</strong> </p>
    </div>

  createNotice: ->
    style =
      color: '#ccc'
      fontSize: '22px'
      textAlign: 'center'

    return <p style={style}>Zadejte alespoň dva body</p>

  render: ->
    rowsOfLengths = @createRows()
    headerRows = if @state.lengths.length isnt 0 then @createHeaderRow()
    resultLength = if @state.resultLength then @createResultRow()
    notice = if @state.lengths.length is 0 then @createNotice() else null

    <div>
      <h2>Měření délek</h2>
      {notice}
      {headerRows}
      {rowsOfLengths}
      {resultLength}
    </div>

module?.exports = appMapPanel
