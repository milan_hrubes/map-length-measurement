(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.MapAppMeasurement = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
var Ol3, PanelComponent, React, VectorLayer, appMap;

PanelComponent = require('./panelComponent.js');

VectorLayer = require('./vectorLayer.js');

React = (typeof window !== "undefined" ? window['React'] : typeof global !== "undefined" ? global['React'] : null);

Ol3 = (typeof window !== "undefined" ? window['ol'] : typeof global !== "undefined" ? global['ol'] : null);

appMap = {

  /**
     * Rendered map
     * @type {ol.Map}
   */
  renderedMap: null,

  /**
     * Rendered layer for vector data
     * @type {Object}
   */
  vectorLayer: null,

  /**
     * React component of panel
     * @type {Class}
   */
  panelComponent: null,

  /*
    * Initialization of map
    *
    * @param {String} target - id of DOM element for map
   */
  init: function(target) {
    this.renderedMap = new Ol3.Map({
      controls: this.setControls(),
      layers: [],
      view: this.setMapView(),
      target: target
    });
    this.setLayers();
    this.createLayerSwitcher();
    return this.panelComponent = React.render(React.createElement(PanelComponent, null), document.getElementById('panel'));
  },

  /*
    * Set inital map view (initial zoom, projection, ..)
    *
    * @return {ol.View}
   */
  setMapView: function() {
    return new Ol3.View({
      center: ol.proj.transform([15, 50], 'EPSG:4326', 'EPSG:3857'),
      zoom: 7,
      projection: 'EPSG:3857'
    });
  },

  /*
    * Set layers of map
    * create layers {ol.layer} and set parameters
   */
  setLayers: function() {
    var baseGroup, osmLayer, overlaysGroup, satelliteLayer;
    osmLayer = new Ol3.layer.Tile({
      title: 'OSM',
      type: 'base',
      visible: true,
      source: new Ol3.source.OSM()
    });
    satelliteLayer = new Ol3.layer.Tile({
      title: 'Satellite',
      type: 'base',
      visible: false,
      source: new Ol3.source.MapQuest({
        layer: 'sat'
      })
    });
    this.vectorLayer = VectorLayer.createLayer(this.renderedMap);
    baseGroup = this.createLayersGroup('Mapy', [osmLayer, satelliteLayer]);
    overlaysGroup = this.createLayersGroup('Overlays', [this.vectorLayer]);
    this.renderedMap.addLayer(baseGroup);
    return this.renderedMap.addLayer(overlaysGroup);
  },

  /*
    * Create group of layers for layer switcher
    *
    * @param {String} groupTitle - title of the group
    * @param {Array} groupLayers - array of created layers
    * @return {ol.layer.Group}
   */
  createLayersGroup: function(groupTitle, groupLayers) {
    return new Ol3.layer.Group({
      title: groupTitle,
      layers: groupLayers
    });
  },

  /*
    * Set controls of the map
    *
    * @return {ol.control.defaults}
   */
  setControls: function() {
    return Ol3.control.defaults().extend([
      new Ol3.control.ScaleLine({
        units: 'metric'
      }), new Ol3.control.ZoomSlider()
    ]);
  },

  /*
    * Create layer switcher to switch layers
   */
  createLayerSwitcher: function() {
    var layerSwitcher;
    layerSwitcher = new Ol3.control.LayerSwitcher({
      tipLabel: 'Legenda'
    });
    return this.renderedMap.addControl(layerSwitcher);
  }
};

if (typeof module !== "undefined" && module !== null) {
  module.exports = appMap;
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./panelComponent.js":4,"./vectorLayer.js":5}],2:[function(require,module,exports){
var constants;

constants = {
  HELPMSG: 'Klikněte pro zadání bodu',
  HELPMSGNEXTPOINT: 'Klikněte pro zadání bodu nebo dvojklikem ukončete',
  WGS84SPHERE: 6378137,
  style: {
    drawTool: {
      FILLCOLOR: 'rgba(255, 255, 255, 0.2)',
      STROKECOLOR: 'rgba(83, 194, 207, 0.8)',
      LINEDASH: [10, 10],
      WIDTH: 4,
      RADIUS: 5
    },
    vectorLayer: {
      FILLCOLOR: 'rgba(83, 194, 207, 0.2)',
      RADIUS: 7,
      STROKECOLOR: 'rgba(255, 255, 255, 0.5)',
      WIDTH: 5,
      STROKECOLORSECOND: 'rgba(83, 194, 207, 0.7)',
      WIDTHSECOND: 3
    }
  }
};

if (typeof module !== "undefined" && module !== null) {
  module.exports = constants;
}

},{}],3:[function(require,module,exports){
(function (global){
var Constants, Ol3, measurementTool;

Ol3 = (typeof window !== "undefined" ? window['ol'] : typeof global !== "undefined" ? global['ol'] : null);

Constants = require('./constants.js');

measurementTool = {

  /**
     * Actual rendering route
     * @type {ol.Feature}
   */
  featureMeasure: null,

  /**
     * DOM element for a label with total measured length
     * @type {Element}
   */
  measureTooltipElement: null,

  /**
     * Layer to display a label with total measured length
     * @type {ol.Overlay}
   */
  measureTooltip: null,

  /**
     * DOM element for a label on cursor Help
     * @type {Element}
   */
  helpTooltipElement: null,

  /**
     * Layer to display a label with Help
     * @type {ol.Overlay}
   */
  helpTooltip: null,

  /**
     * Tool for drawing lines
     * @type {ol.interaction.Draw}
   */
  drawTool: null,
  init: function(renderedMap, vectorLayer) {
    this.renderedMap = renderedMap;
    this.vectorLayer = vectorLayer;
    if (!this.drawTool) {
      this.renderedMap.on('pointermove', this.pointerMoveHandler);
      return this.addInteraction();
    }
  },
  pointerMoveHandler: function(ev) {
    var geom, output, toolTipMsg, tooltipCoord;
    if (ev.dragging) {
      return;
    }
    tooltipCoord = ev.coordinate;
    toolTipMsg = Constants.HELPMSG;
    if (measurementTool.featureMeasure) {
      output = void 0;
      geom = measurementTool.featureMeasure.getGeometry();
      if (geom instanceof Ol3.geom.LineString) {
        output = measurementTool.formatLengthLineString(geom);
        tooltipCoord = geom.getLastCoordinate();
        toolTipMsg = Constants.HELPMSGNEXTPOINT;
      }
      measurementTool.measureTooltipElement.innerHTML = output;
      measurementTool.measureTooltip.setPosition(tooltipCoord);
    }
    measurementTool.helpTooltipElement.innerHTML = toolTipMsg;
    return measurementTool.helpTooltip.setPosition(ev.coordinate);
  },
  addInteraction: function() {
    this.drawTool = new Ol3.interaction.Draw({
      source: this.vectorLayer.getSource(),
      type: 'LineString',
      style: this.createStyle()
    });
    this.renderedMap.addInteraction(this.drawTool);
    this.createMeasureTooltip();
    this.createHelpTooltip();
    this.renderedMap.on('singleclick', function(ev) {
      return measurementTool.addedSegment('drawContinuing');
    });
    this.drawTool.on('drawstart', this.drawStart);
    return this.drawTool.on('drawend', this.drawEnd);
  },
  createStyle: function() {
    return new Ol3.style.Style({
      fill: new Ol3.style.Fill({
        color: Constants.style.drawTool.FILLCOLOR
      }),
      stroke: new Ol3.style.Stroke({
        color: Constants.style.drawTool.STROKECOLOR,
        lineDash: Constants.style.drawTool.LINEDASH,
        width: Constants.style.drawTool.WIDTH
      }),
      image: new Ol3.style.Circle({
        radius: Constants.style.drawTool.RADIUS,
        stroke: new Ol3.style.Stroke({
          color: Constants.style.drawTool.STROKECOLOR
        }),
        fill: new Ol3.style.Fill({
          color: Constants.style.drawTool.FILLCOLOR
        })
      })
    });
  },
  addedSegment: function(type) {
    var coordinates, index, length, lengths, ref;
    index = 0;
    lengths = [];
    coordinates = (ref = measurementTool.featureMeasure) != null ? ref.getGeometry().getCoordinates() : void 0;
    if (type === 'drawContinuing') {
      if (coordinates != null) {
        coordinates.pop();
      }
    }
    while (index < (coordinates != null ? coordinates.length : void 0)) {
      if (coordinates[index + 1]) {
        length = this.formatLengthTransform(coordinates[index], coordinates[index + 1]);
        if (length !== 0) {
          lengths.push(this.formatCompleteLength(length));
        }
      }
      ++index;
    }
    return MapAppMeasurement.panelComponent.setState({
      lengths: lengths
    });
  },
  drawStart: function(ev) {
    measurementTool.featureMeasure = ev.feature;
    return measurementTool.clearPreviousMeasure();
  },
  drawEnd: function(ev) {
    var resultLengthValue;
    measurementTool.addedSegment('drawEnd');
    resultLengthValue = measurementTool.measureTooltipElement.innerHTML;
    MapAppMeasurement.panelComponent.setState({
      resultLength: resultLengthValue
    });
    measurementTool.measureTooltipElement.id = 'result-tooltip';
    measurementTool.measureTooltipElement.className = 'tooltip tooltip-static';
    measurementTool.measureTooltip.setOffset([0, -7]);
    measurementTool.featureMeasure = null;
    measurementTool.measureTooltipElement = null;
    return measurementTool.createMeasureTooltip();
  },
  clearPreviousMeasure: function() {
    var overlays;
    overlays = this.renderedMap.getOverlays();
    overlays.forEach(function(overlay) {
      if (overlay.getElement().id === 'result-tooltip') {
        return overlays.remove(overlay);
      }
    });
    this.vectorLayer.getSource().clear();
    return MapAppMeasurement.panelComponent.setState({
      resultLength: null,
      lengths: []
    });
  },
  createHelpTooltip: function() {
    if (this.helpTooltipElement) {
      this.helpTooltipElement.parentNode.removeChild(this.helpTooltipElement);
    }
    this.helpTooltipElement = this.createDivElement('tooltip');
    this.helpTooltip = this.createOverlay(this.helpTooltipElement, [15, 0], 'center-left');
    return this.renderedMap.addOverlay(this.helpTooltip);
  },
  createMeasureTooltip: function() {
    if (this.measureTooltipElement) {
      this.measureTooltipElement.parentNode.removeChild(this.measureTooltipElement);
    }
    this.measureTooltipElement = this.createDivElement('tooltip tooltip-measure');
    this.measureTooltip = this.createOverlay(this.measureTooltipElement, [0, -15], 'bottom-center');
    return this.renderedMap.addOverlay(this.measureTooltip);
  },
  createDivElement: function(className) {
    var element;
    element = document.createElement('div');
    element.className = className;
    return element;
  },
  createOverlay: function(element, offset, position) {
    return new Ol3.Overlay({
      element: element,
      offset: offset,
      positioning: position
    });
  },
  formatLengthLineString: function(line) {
    var coordinates, index, len, length, sourceProj;
    coordinates = line.getCoordinates();
    sourceProj = this.renderedMap.getView().getProjection();
    length = 0;
    index = 0;
    len = coordinates.length - 1;
    while (index < len) {
      length += this.formatLengthTransform(coordinates[index], coordinates[index + 1]);
      ++index;
    }
    return this.formatCompleteLength(length);
  },
  formatLengthTransform: function(coord1, coord2) {
    var c1, c2, sourceProj, wgs84Sphere;
    wgs84Sphere = new Ol3.Sphere(Constants.WGS84SPHERE);
    sourceProj = this.renderedMap.getView().getProjection();
    c1 = Ol3.proj.transform(coord1, sourceProj, 'EPSG:4326');
    c2 = Ol3.proj.transform(coord2, sourceProj, 'EPSG:4326');
    return wgs84Sphere.haversineDistance(c1, c2);
  },
  formatCompleteLength: function(length) {
    var formatedLength;
    if (length > 100) {
      formatedLength = Math.round(length / 1000 * 100) / 100 + ' ' + 'km';
    } else {
      formatedLength = Math.round(length * 100) / 100 + ' ' + 'm';
    }
    return formatedLength;
  }
};

if (typeof module !== "undefined" && module !== null) {
  module.exports = measurementTool;
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./constants.js":2}],4:[function(require,module,exports){
(function (global){
var React, appMapPanel;

React = (typeof window !== "undefined" ? window['React'] : typeof global !== "undefined" ? global['React'] : null);

appMapPanel = React.createClass({
  displayName: 'PanelComponent',
  getInitialState: function() {
    return {
      lengths: [],
      showLengths: false,
      resultLength: ''
    };
  },
  createRows: function() {
    return this.state.lengths.map(function(item, i) {
      return React.createElement("div", {
        "className": 'row'
      }, React.createElement("span", null, i + 1, ".", item));
    });
  },
  createHeaderRow: function() {
    return React.createElement("div", {
      "className": 'row-header'
    }, React.createElement("span", null, "Úsek"), React.createElement("span", null, "Vzdálenost"));
  },
  createResultRow: function() {
    return React.createElement("div", {
      "className": 'resultLength'
    }, React.createElement("span", null, "Celková délka trasy"), React.createElement("p", null, " ", React.createElement("strong", null, this.state.resultLength), " "));
  },
  createNotice: function() {
    var style;
    style = {
      color: '#ccc',
      fontSize: '22px',
      textAlign: 'center'
    };
    return React.createElement("p", {
      "style": style
    }, "Zadejte alespoň dva body");
  },
  render: function() {
    var headerRows, notice, resultLength, rowsOfLengths;
    rowsOfLengths = this.createRows();
    headerRows = this.state.lengths.length !== 0 ? this.createHeaderRow() : void 0;
    resultLength = this.state.resultLength ? this.createResultRow() : void 0;
    notice = this.state.lengths.length === 0 ? this.createNotice() : null;
    return React.createElement("div", null, React.createElement("h2", null, "Měření délek"), notice, headerRows, rowsOfLengths, resultLength);
  }
});

if (typeof module !== "undefined" && module !== null) {
  module.exports = appMapPanel;
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],5:[function(require,module,exports){
(function (global){
var Constants, MeasurementTool, Ol3, vectorLayer;

Ol3 = (typeof window !== "undefined" ? window['ol'] : typeof global !== "undefined" ? global['ol'] : null);

MeasurementTool = require('./measurementTool.js');

Constants = require('./constants.js');

vectorLayer = {

  /**
     * Rendered vector layer
     * @type {ol.layer.Vector}
   */
  layer: null,

  /**
     * Rendered map
     * @type {ol.Map}
   */
  renderedMap: null,

  /*
    * Initialization of the layer
    *
    * @param {ol.Map} renderedMap - rendered ol3 map
    * @return {ol.layer.Vector}
   */
  createLayer: function(renderedMap) {
    this.renderedMap = renderedMap;
    this.layer = new Ol3.layer.Vector({
      title: 'Vector',
      source: new Ol3.source.Vector(),
      style: this.createStyles()
    });
    MeasurementTool.init(this.renderedMap, this.layer);
    return this.layer;
  },

  /*
    * Create styles of vector layer
    *
    * @return {Array}
   */
  createStyles: function() {
    return [
      new Ol3.style.Style({
        fill: new Ol3.style.Fill({
          color: Constants.style.vectorLayer.FILLCOLOR
        }),
        stroke: new Ol3.style.Stroke({
          color: Constants.style.vectorLayer.STROKECOLOR,
          width: Constants.style.vectorLayer.WIDTH
        }),
        image: new Ol3.style.Circle({
          radius: Constants.style.vectorLayer.RADIUS,
          fill: new Ol3.style.Fill({
            color: Constants.style.vectorLayer.FILLCOLOR
          })
        })
      }), new Ol3.style.Style({
        stroke: new Ol3.style.Stroke({
          color: Constants.style.vectorLayer.STROKECOLORSECOND,
          width: Constants.style.vectorLayer.WIDTHSECOND
        })
      })
    ];
  }
};

if (typeof module !== "undefined" && module !== null) {
  module.exports = vectorLayer;
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./constants.js":2,"./measurementTool.js":3}]},{},[1])(1)
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvYXBwLmpzIiwic3JjL2pzL2NvbnN0YW50cy5qcyIsInNyYy9qcy9tZWFzdXJlbWVudFRvb2wuanMiLCJzcmMvanMvcGFuZWxDb21wb25lbnQuanMiLCJzcmMvanMvdmVjdG9yTGF5ZXIuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUNqSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FDNUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUNuT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FDdERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwidmFyIE9sMywgUGFuZWxDb21wb25lbnQsIFJlYWN0LCBWZWN0b3JMYXllciwgYXBwTWFwO1xuXG5QYW5lbENvbXBvbmVudCA9IHJlcXVpcmUoJy4vcGFuZWxDb21wb25lbnQuanMnKTtcblxuVmVjdG9yTGF5ZXIgPSByZXF1aXJlKCcuL3ZlY3RvckxheWVyLmpzJyk7XG5cblJlYWN0ID0gKHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3dbJ1JlYWN0J10gOiB0eXBlb2YgZ2xvYmFsICE9PSBcInVuZGVmaW5lZFwiID8gZ2xvYmFsWydSZWFjdCddIDogbnVsbCk7XG5cbk9sMyA9ICh0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93WydvbCddIDogdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbFsnb2wnXSA6IG51bGwpO1xuXG5hcHBNYXAgPSB7XG5cbiAgLyoqXG4gICAgICogUmVuZGVyZWQgbWFwXG4gICAgICogQHR5cGUge29sLk1hcH1cbiAgICovXG4gIHJlbmRlcmVkTWFwOiBudWxsLFxuXG4gIC8qKlxuICAgICAqIFJlbmRlcmVkIGxheWVyIGZvciB2ZWN0b3IgZGF0YVxuICAgICAqIEB0eXBlIHtPYmplY3R9XG4gICAqL1xuICB2ZWN0b3JMYXllcjogbnVsbCxcblxuICAvKipcbiAgICAgKiBSZWFjdCBjb21wb25lbnQgb2YgcGFuZWxcbiAgICAgKiBAdHlwZSB7Q2xhc3N9XG4gICAqL1xuICBwYW5lbENvbXBvbmVudDogbnVsbCxcblxuICAvKlxuICAgICogSW5pdGlhbGl6YXRpb24gb2YgbWFwXG4gICAgKlxuICAgICogQHBhcmFtIHtTdHJpbmd9IHRhcmdldCAtIGlkIG9mIERPTSBlbGVtZW50IGZvciBtYXBcbiAgICovXG4gIGluaXQ6IGZ1bmN0aW9uKHRhcmdldCkge1xuICAgIHRoaXMucmVuZGVyZWRNYXAgPSBuZXcgT2wzLk1hcCh7XG4gICAgICBjb250cm9sczogdGhpcy5zZXRDb250cm9scygpLFxuICAgICAgbGF5ZXJzOiBbXSxcbiAgICAgIHZpZXc6IHRoaXMuc2V0TWFwVmlldygpLFxuICAgICAgdGFyZ2V0OiB0YXJnZXRcbiAgICB9KTtcbiAgICB0aGlzLnNldExheWVycygpO1xuICAgIHRoaXMuY3JlYXRlTGF5ZXJTd2l0Y2hlcigpO1xuICAgIHJldHVybiB0aGlzLnBhbmVsQ29tcG9uZW50ID0gUmVhY3QucmVuZGVyKFJlYWN0LmNyZWF0ZUVsZW1lbnQoUGFuZWxDb21wb25lbnQsIG51bGwpLCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgncGFuZWwnKSk7XG4gIH0sXG5cbiAgLypcbiAgICAqIFNldCBpbml0YWwgbWFwIHZpZXcgKGluaXRpYWwgem9vbSwgcHJvamVjdGlvbiwgLi4pXG4gICAgKlxuICAgICogQHJldHVybiB7b2wuVmlld31cbiAgICovXG4gIHNldE1hcFZpZXc6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiBuZXcgT2wzLlZpZXcoe1xuICAgICAgY2VudGVyOiBvbC5wcm9qLnRyYW5zZm9ybShbMTUsIDUwXSwgJ0VQU0c6NDMyNicsICdFUFNHOjM4NTcnKSxcbiAgICAgIHpvb206IDcsXG4gICAgICBwcm9qZWN0aW9uOiAnRVBTRzozODU3J1xuICAgIH0pO1xuICB9LFxuXG4gIC8qXG4gICAgKiBTZXQgbGF5ZXJzIG9mIG1hcFxuICAgICogY3JlYXRlIGxheWVycyB7b2wubGF5ZXJ9IGFuZCBzZXQgcGFyYW1ldGVyc1xuICAgKi9cbiAgc2V0TGF5ZXJzOiBmdW5jdGlvbigpIHtcbiAgICB2YXIgYmFzZUdyb3VwLCBvc21MYXllciwgb3ZlcmxheXNHcm91cCwgc2F0ZWxsaXRlTGF5ZXI7XG4gICAgb3NtTGF5ZXIgPSBuZXcgT2wzLmxheWVyLlRpbGUoe1xuICAgICAgdGl0bGU6ICdPU00nLFxuICAgICAgdHlwZTogJ2Jhc2UnLFxuICAgICAgdmlzaWJsZTogdHJ1ZSxcbiAgICAgIHNvdXJjZTogbmV3IE9sMy5zb3VyY2UuT1NNKClcbiAgICB9KTtcbiAgICBzYXRlbGxpdGVMYXllciA9IG5ldyBPbDMubGF5ZXIuVGlsZSh7XG4gICAgICB0aXRsZTogJ1NhdGVsbGl0ZScsXG4gICAgICB0eXBlOiAnYmFzZScsXG4gICAgICB2aXNpYmxlOiBmYWxzZSxcbiAgICAgIHNvdXJjZTogbmV3IE9sMy5zb3VyY2UuTWFwUXVlc3Qoe1xuICAgICAgICBsYXllcjogJ3NhdCdcbiAgICAgIH0pXG4gICAgfSk7XG4gICAgdGhpcy52ZWN0b3JMYXllciA9IFZlY3RvckxheWVyLmNyZWF0ZUxheWVyKHRoaXMucmVuZGVyZWRNYXApO1xuICAgIGJhc2VHcm91cCA9IHRoaXMuY3JlYXRlTGF5ZXJzR3JvdXAoJ01hcHknLCBbb3NtTGF5ZXIsIHNhdGVsbGl0ZUxheWVyXSk7XG4gICAgb3ZlcmxheXNHcm91cCA9IHRoaXMuY3JlYXRlTGF5ZXJzR3JvdXAoJ092ZXJsYXlzJywgW3RoaXMudmVjdG9yTGF5ZXJdKTtcbiAgICB0aGlzLnJlbmRlcmVkTWFwLmFkZExheWVyKGJhc2VHcm91cCk7XG4gICAgcmV0dXJuIHRoaXMucmVuZGVyZWRNYXAuYWRkTGF5ZXIob3ZlcmxheXNHcm91cCk7XG4gIH0sXG5cbiAgLypcbiAgICAqIENyZWF0ZSBncm91cCBvZiBsYXllcnMgZm9yIGxheWVyIHN3aXRjaGVyXG4gICAgKlxuICAgICogQHBhcmFtIHtTdHJpbmd9IGdyb3VwVGl0bGUgLSB0aXRsZSBvZiB0aGUgZ3JvdXBcbiAgICAqIEBwYXJhbSB7QXJyYXl9IGdyb3VwTGF5ZXJzIC0gYXJyYXkgb2YgY3JlYXRlZCBsYXllcnNcbiAgICAqIEByZXR1cm4ge29sLmxheWVyLkdyb3VwfVxuICAgKi9cbiAgY3JlYXRlTGF5ZXJzR3JvdXA6IGZ1bmN0aW9uKGdyb3VwVGl0bGUsIGdyb3VwTGF5ZXJzKSB7XG4gICAgcmV0dXJuIG5ldyBPbDMubGF5ZXIuR3JvdXAoe1xuICAgICAgdGl0bGU6IGdyb3VwVGl0bGUsXG4gICAgICBsYXllcnM6IGdyb3VwTGF5ZXJzXG4gICAgfSk7XG4gIH0sXG5cbiAgLypcbiAgICAqIFNldCBjb250cm9scyBvZiB0aGUgbWFwXG4gICAgKlxuICAgICogQHJldHVybiB7b2wuY29udHJvbC5kZWZhdWx0c31cbiAgICovXG4gIHNldENvbnRyb2xzOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gT2wzLmNvbnRyb2wuZGVmYXVsdHMoKS5leHRlbmQoW1xuICAgICAgbmV3IE9sMy5jb250cm9sLlNjYWxlTGluZSh7XG4gICAgICAgIHVuaXRzOiAnbWV0cmljJ1xuICAgICAgfSksIG5ldyBPbDMuY29udHJvbC5ab29tU2xpZGVyKClcbiAgICBdKTtcbiAgfSxcblxuICAvKlxuICAgICogQ3JlYXRlIGxheWVyIHN3aXRjaGVyIHRvIHN3aXRjaCBsYXllcnNcbiAgICovXG4gIGNyZWF0ZUxheWVyU3dpdGNoZXI6IGZ1bmN0aW9uKCkge1xuICAgIHZhciBsYXllclN3aXRjaGVyO1xuICAgIGxheWVyU3dpdGNoZXIgPSBuZXcgT2wzLmNvbnRyb2wuTGF5ZXJTd2l0Y2hlcih7XG4gICAgICB0aXBMYWJlbDogJ0xlZ2VuZGEnXG4gICAgfSk7XG4gICAgcmV0dXJuIHRoaXMucmVuZGVyZWRNYXAuYWRkQ29udHJvbChsYXllclN3aXRjaGVyKTtcbiAgfVxufTtcblxuaWYgKHR5cGVvZiBtb2R1bGUgIT09IFwidW5kZWZpbmVkXCIgJiYgbW9kdWxlICE9PSBudWxsKSB7XG4gIG1vZHVsZS5leHBvcnRzID0gYXBwTWFwO1xufVxuIiwidmFyIGNvbnN0YW50cztcblxuY29uc3RhbnRzID0ge1xuICBIRUxQTVNHOiAnS2xpa27Em3RlIHBybyB6YWTDoW7DrSBib2R1JyxcbiAgSEVMUE1TR05FWFRQT0lOVDogJ0tsaWtuxJt0ZSBwcm8gemFkw6Fuw60gYm9kdSBuZWJvIGR2b2prbGlrZW0gdWtvbsSNZXRlJyxcbiAgV0dTODRTUEhFUkU6IDYzNzgxMzcsXG4gIHN0eWxlOiB7XG4gICAgZHJhd1Rvb2w6IHtcbiAgICAgIEZJTExDT0xPUjogJ3JnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKScsXG4gICAgICBTVFJPS0VDT0xPUjogJ3JnYmEoODMsIDE5NCwgMjA3LCAwLjgpJyxcbiAgICAgIExJTkVEQVNIOiBbMTAsIDEwXSxcbiAgICAgIFdJRFRIOiA0LFxuICAgICAgUkFESVVTOiA1XG4gICAgfSxcbiAgICB2ZWN0b3JMYXllcjoge1xuICAgICAgRklMTENPTE9SOiAncmdiYSg4MywgMTk0LCAyMDcsIDAuMiknLFxuICAgICAgUkFESVVTOiA3LFxuICAgICAgU1RST0tFQ09MT1I6ICdyZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNSknLFxuICAgICAgV0lEVEg6IDUsXG4gICAgICBTVFJPS0VDT0xPUlNFQ09ORDogJ3JnYmEoODMsIDE5NCwgMjA3LCAwLjcpJyxcbiAgICAgIFdJRFRIU0VDT05EOiAzXG4gICAgfVxuICB9XG59O1xuXG5pZiAodHlwZW9mIG1vZHVsZSAhPT0gXCJ1bmRlZmluZWRcIiAmJiBtb2R1bGUgIT09IG51bGwpIHtcbiAgbW9kdWxlLmV4cG9ydHMgPSBjb25zdGFudHM7XG59XG4iLCJ2YXIgQ29uc3RhbnRzLCBPbDMsIG1lYXN1cmVtZW50VG9vbDtcblxuT2wzID0gKHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3dbJ29sJ10gOiB0eXBlb2YgZ2xvYmFsICE9PSBcInVuZGVmaW5lZFwiID8gZ2xvYmFsWydvbCddIDogbnVsbCk7XG5cbkNvbnN0YW50cyA9IHJlcXVpcmUoJy4vY29uc3RhbnRzLmpzJyk7XG5cbm1lYXN1cmVtZW50VG9vbCA9IHtcblxuICAvKipcbiAgICAgKiBBY3R1YWwgcmVuZGVyaW5nIHJvdXRlXG4gICAgICogQHR5cGUge29sLkZlYXR1cmV9XG4gICAqL1xuICBmZWF0dXJlTWVhc3VyZTogbnVsbCxcblxuICAvKipcbiAgICAgKiBET00gZWxlbWVudCBmb3IgYSBsYWJlbCB3aXRoIHRvdGFsIG1lYXN1cmVkIGxlbmd0aFxuICAgICAqIEB0eXBlIHtFbGVtZW50fVxuICAgKi9cbiAgbWVhc3VyZVRvb2x0aXBFbGVtZW50OiBudWxsLFxuXG4gIC8qKlxuICAgICAqIExheWVyIHRvIGRpc3BsYXkgYSBsYWJlbCB3aXRoIHRvdGFsIG1lYXN1cmVkIGxlbmd0aFxuICAgICAqIEB0eXBlIHtvbC5PdmVybGF5fVxuICAgKi9cbiAgbWVhc3VyZVRvb2x0aXA6IG51bGwsXG5cbiAgLyoqXG4gICAgICogRE9NIGVsZW1lbnQgZm9yIGEgbGFiZWwgb24gY3Vyc29yIEhlbHBcbiAgICAgKiBAdHlwZSB7RWxlbWVudH1cbiAgICovXG4gIGhlbHBUb29sdGlwRWxlbWVudDogbnVsbCxcblxuICAvKipcbiAgICAgKiBMYXllciB0byBkaXNwbGF5IGEgbGFiZWwgd2l0aCBIZWxwXG4gICAgICogQHR5cGUge29sLk92ZXJsYXl9XG4gICAqL1xuICBoZWxwVG9vbHRpcDogbnVsbCxcblxuICAvKipcbiAgICAgKiBUb29sIGZvciBkcmF3aW5nIGxpbmVzXG4gICAgICogQHR5cGUge29sLmludGVyYWN0aW9uLkRyYXd9XG4gICAqL1xuICBkcmF3VG9vbDogbnVsbCxcbiAgaW5pdDogZnVuY3Rpb24ocmVuZGVyZWRNYXAsIHZlY3RvckxheWVyKSB7XG4gICAgdGhpcy5yZW5kZXJlZE1hcCA9IHJlbmRlcmVkTWFwO1xuICAgIHRoaXMudmVjdG9yTGF5ZXIgPSB2ZWN0b3JMYXllcjtcbiAgICBpZiAoIXRoaXMuZHJhd1Rvb2wpIHtcbiAgICAgIHRoaXMucmVuZGVyZWRNYXAub24oJ3BvaW50ZXJtb3ZlJywgdGhpcy5wb2ludGVyTW92ZUhhbmRsZXIpO1xuICAgICAgcmV0dXJuIHRoaXMuYWRkSW50ZXJhY3Rpb24oKTtcbiAgICB9XG4gIH0sXG4gIHBvaW50ZXJNb3ZlSGFuZGxlcjogZnVuY3Rpb24oZXYpIHtcbiAgICB2YXIgZ2VvbSwgb3V0cHV0LCB0b29sVGlwTXNnLCB0b29sdGlwQ29vcmQ7XG4gICAgaWYgKGV2LmRyYWdnaW5nKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRvb2x0aXBDb29yZCA9IGV2LmNvb3JkaW5hdGU7XG4gICAgdG9vbFRpcE1zZyA9IENvbnN0YW50cy5IRUxQTVNHO1xuICAgIGlmIChtZWFzdXJlbWVudFRvb2wuZmVhdHVyZU1lYXN1cmUpIHtcbiAgICAgIG91dHB1dCA9IHZvaWQgMDtcbiAgICAgIGdlb20gPSBtZWFzdXJlbWVudFRvb2wuZmVhdHVyZU1lYXN1cmUuZ2V0R2VvbWV0cnkoKTtcbiAgICAgIGlmIChnZW9tIGluc3RhbmNlb2YgT2wzLmdlb20uTGluZVN0cmluZykge1xuICAgICAgICBvdXRwdXQgPSBtZWFzdXJlbWVudFRvb2wuZm9ybWF0TGVuZ3RoTGluZVN0cmluZyhnZW9tKTtcbiAgICAgICAgdG9vbHRpcENvb3JkID0gZ2VvbS5nZXRMYXN0Q29vcmRpbmF0ZSgpO1xuICAgICAgICB0b29sVGlwTXNnID0gQ29uc3RhbnRzLkhFTFBNU0dORVhUUE9JTlQ7XG4gICAgICB9XG4gICAgICBtZWFzdXJlbWVudFRvb2wubWVhc3VyZVRvb2x0aXBFbGVtZW50LmlubmVySFRNTCA9IG91dHB1dDtcbiAgICAgIG1lYXN1cmVtZW50VG9vbC5tZWFzdXJlVG9vbHRpcC5zZXRQb3NpdGlvbih0b29sdGlwQ29vcmQpO1xuICAgIH1cbiAgICBtZWFzdXJlbWVudFRvb2wuaGVscFRvb2x0aXBFbGVtZW50LmlubmVySFRNTCA9IHRvb2xUaXBNc2c7XG4gICAgcmV0dXJuIG1lYXN1cmVtZW50VG9vbC5oZWxwVG9vbHRpcC5zZXRQb3NpdGlvbihldi5jb29yZGluYXRlKTtcbiAgfSxcbiAgYWRkSW50ZXJhY3Rpb246IGZ1bmN0aW9uKCkge1xuICAgIHRoaXMuZHJhd1Rvb2wgPSBuZXcgT2wzLmludGVyYWN0aW9uLkRyYXcoe1xuICAgICAgc291cmNlOiB0aGlzLnZlY3RvckxheWVyLmdldFNvdXJjZSgpLFxuICAgICAgdHlwZTogJ0xpbmVTdHJpbmcnLFxuICAgICAgc3R5bGU6IHRoaXMuY3JlYXRlU3R5bGUoKVxuICAgIH0pO1xuICAgIHRoaXMucmVuZGVyZWRNYXAuYWRkSW50ZXJhY3Rpb24odGhpcy5kcmF3VG9vbCk7XG4gICAgdGhpcy5jcmVhdGVNZWFzdXJlVG9vbHRpcCgpO1xuICAgIHRoaXMuY3JlYXRlSGVscFRvb2x0aXAoKTtcbiAgICB0aGlzLnJlbmRlcmVkTWFwLm9uKCdzaW5nbGVjbGljaycsIGZ1bmN0aW9uKGV2KSB7XG4gICAgICByZXR1cm4gbWVhc3VyZW1lbnRUb29sLmFkZGVkU2VnbWVudCgnZHJhd0NvbnRpbnVpbmcnKTtcbiAgICB9KTtcbiAgICB0aGlzLmRyYXdUb29sLm9uKCdkcmF3c3RhcnQnLCB0aGlzLmRyYXdTdGFydCk7XG4gICAgcmV0dXJuIHRoaXMuZHJhd1Rvb2wub24oJ2RyYXdlbmQnLCB0aGlzLmRyYXdFbmQpO1xuICB9LFxuICBjcmVhdGVTdHlsZTogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIG5ldyBPbDMuc3R5bGUuU3R5bGUoe1xuICAgICAgZmlsbDogbmV3IE9sMy5zdHlsZS5GaWxsKHtcbiAgICAgICAgY29sb3I6IENvbnN0YW50cy5zdHlsZS5kcmF3VG9vbC5GSUxMQ09MT1JcbiAgICAgIH0pLFxuICAgICAgc3Ryb2tlOiBuZXcgT2wzLnN0eWxlLlN0cm9rZSh7XG4gICAgICAgIGNvbG9yOiBDb25zdGFudHMuc3R5bGUuZHJhd1Rvb2wuU1RST0tFQ09MT1IsXG4gICAgICAgIGxpbmVEYXNoOiBDb25zdGFudHMuc3R5bGUuZHJhd1Rvb2wuTElORURBU0gsXG4gICAgICAgIHdpZHRoOiBDb25zdGFudHMuc3R5bGUuZHJhd1Rvb2wuV0lEVEhcbiAgICAgIH0pLFxuICAgICAgaW1hZ2U6IG5ldyBPbDMuc3R5bGUuQ2lyY2xlKHtcbiAgICAgICAgcmFkaXVzOiBDb25zdGFudHMuc3R5bGUuZHJhd1Rvb2wuUkFESVVTLFxuICAgICAgICBzdHJva2U6IG5ldyBPbDMuc3R5bGUuU3Ryb2tlKHtcbiAgICAgICAgICBjb2xvcjogQ29uc3RhbnRzLnN0eWxlLmRyYXdUb29sLlNUUk9LRUNPTE9SXG4gICAgICAgIH0pLFxuICAgICAgICBmaWxsOiBuZXcgT2wzLnN0eWxlLkZpbGwoe1xuICAgICAgICAgIGNvbG9yOiBDb25zdGFudHMuc3R5bGUuZHJhd1Rvb2wuRklMTENPTE9SXG4gICAgICAgIH0pXG4gICAgICB9KVxuICAgIH0pO1xuICB9LFxuICBhZGRlZFNlZ21lbnQ6IGZ1bmN0aW9uKHR5cGUpIHtcbiAgICB2YXIgY29vcmRpbmF0ZXMsIGluZGV4LCBsZW5ndGgsIGxlbmd0aHMsIHJlZjtcbiAgICBpbmRleCA9IDA7XG4gICAgbGVuZ3RocyA9IFtdO1xuICAgIGNvb3JkaW5hdGVzID0gKHJlZiA9IG1lYXN1cmVtZW50VG9vbC5mZWF0dXJlTWVhc3VyZSkgIT0gbnVsbCA/IHJlZi5nZXRHZW9tZXRyeSgpLmdldENvb3JkaW5hdGVzKCkgOiB2b2lkIDA7XG4gICAgaWYgKHR5cGUgPT09ICdkcmF3Q29udGludWluZycpIHtcbiAgICAgIGlmIChjb29yZGluYXRlcyAhPSBudWxsKSB7XG4gICAgICAgIGNvb3JkaW5hdGVzLnBvcCgpO1xuICAgICAgfVxuICAgIH1cbiAgICB3aGlsZSAoaW5kZXggPCAoY29vcmRpbmF0ZXMgIT0gbnVsbCA/IGNvb3JkaW5hdGVzLmxlbmd0aCA6IHZvaWQgMCkpIHtcbiAgICAgIGlmIChjb29yZGluYXRlc1tpbmRleCArIDFdKSB7XG4gICAgICAgIGxlbmd0aCA9IHRoaXMuZm9ybWF0TGVuZ3RoVHJhbnNmb3JtKGNvb3JkaW5hdGVzW2luZGV4XSwgY29vcmRpbmF0ZXNbaW5kZXggKyAxXSk7XG4gICAgICAgIGlmIChsZW5ndGggIT09IDApIHtcbiAgICAgICAgICBsZW5ndGhzLnB1c2godGhpcy5mb3JtYXRDb21wbGV0ZUxlbmd0aChsZW5ndGgpKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgKytpbmRleDtcbiAgICB9XG4gICAgcmV0dXJuIE1hcEFwcE1lYXN1cmVtZW50LnBhbmVsQ29tcG9uZW50LnNldFN0YXRlKHtcbiAgICAgIGxlbmd0aHM6IGxlbmd0aHNcbiAgICB9KTtcbiAgfSxcbiAgZHJhd1N0YXJ0OiBmdW5jdGlvbihldikge1xuICAgIG1lYXN1cmVtZW50VG9vbC5mZWF0dXJlTWVhc3VyZSA9IGV2LmZlYXR1cmU7XG4gICAgcmV0dXJuIG1lYXN1cmVtZW50VG9vbC5jbGVhclByZXZpb3VzTWVhc3VyZSgpO1xuICB9LFxuICBkcmF3RW5kOiBmdW5jdGlvbihldikge1xuICAgIHZhciByZXN1bHRMZW5ndGhWYWx1ZTtcbiAgICBtZWFzdXJlbWVudFRvb2wuYWRkZWRTZWdtZW50KCdkcmF3RW5kJyk7XG4gICAgcmVzdWx0TGVuZ3RoVmFsdWUgPSBtZWFzdXJlbWVudFRvb2wubWVhc3VyZVRvb2x0aXBFbGVtZW50LmlubmVySFRNTDtcbiAgICBNYXBBcHBNZWFzdXJlbWVudC5wYW5lbENvbXBvbmVudC5zZXRTdGF0ZSh7XG4gICAgICByZXN1bHRMZW5ndGg6IHJlc3VsdExlbmd0aFZhbHVlXG4gICAgfSk7XG4gICAgbWVhc3VyZW1lbnRUb29sLm1lYXN1cmVUb29sdGlwRWxlbWVudC5pZCA9ICdyZXN1bHQtdG9vbHRpcCc7XG4gICAgbWVhc3VyZW1lbnRUb29sLm1lYXN1cmVUb29sdGlwRWxlbWVudC5jbGFzc05hbWUgPSAndG9vbHRpcCB0b29sdGlwLXN0YXRpYyc7XG4gICAgbWVhc3VyZW1lbnRUb29sLm1lYXN1cmVUb29sdGlwLnNldE9mZnNldChbMCwgLTddKTtcbiAgICBtZWFzdXJlbWVudFRvb2wuZmVhdHVyZU1lYXN1cmUgPSBudWxsO1xuICAgIG1lYXN1cmVtZW50VG9vbC5tZWFzdXJlVG9vbHRpcEVsZW1lbnQgPSBudWxsO1xuICAgIHJldHVybiBtZWFzdXJlbWVudFRvb2wuY3JlYXRlTWVhc3VyZVRvb2x0aXAoKTtcbiAgfSxcbiAgY2xlYXJQcmV2aW91c01lYXN1cmU6IGZ1bmN0aW9uKCkge1xuICAgIHZhciBvdmVybGF5cztcbiAgICBvdmVybGF5cyA9IHRoaXMucmVuZGVyZWRNYXAuZ2V0T3ZlcmxheXMoKTtcbiAgICBvdmVybGF5cy5mb3JFYWNoKGZ1bmN0aW9uKG92ZXJsYXkpIHtcbiAgICAgIGlmIChvdmVybGF5LmdldEVsZW1lbnQoKS5pZCA9PT0gJ3Jlc3VsdC10b29sdGlwJykge1xuICAgICAgICByZXR1cm4gb3ZlcmxheXMucmVtb3ZlKG92ZXJsYXkpO1xuICAgICAgfVxuICAgIH0pO1xuICAgIHRoaXMudmVjdG9yTGF5ZXIuZ2V0U291cmNlKCkuY2xlYXIoKTtcbiAgICByZXR1cm4gTWFwQXBwTWVhc3VyZW1lbnQucGFuZWxDb21wb25lbnQuc2V0U3RhdGUoe1xuICAgICAgcmVzdWx0TGVuZ3RoOiBudWxsLFxuICAgICAgbGVuZ3RoczogW11cbiAgICB9KTtcbiAgfSxcbiAgY3JlYXRlSGVscFRvb2x0aXA6IGZ1bmN0aW9uKCkge1xuICAgIGlmICh0aGlzLmhlbHBUb29sdGlwRWxlbWVudCkge1xuICAgICAgdGhpcy5oZWxwVG9vbHRpcEVsZW1lbnQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZCh0aGlzLmhlbHBUb29sdGlwRWxlbWVudCk7XG4gICAgfVxuICAgIHRoaXMuaGVscFRvb2x0aXBFbGVtZW50ID0gdGhpcy5jcmVhdGVEaXZFbGVtZW50KCd0b29sdGlwJyk7XG4gICAgdGhpcy5oZWxwVG9vbHRpcCA9IHRoaXMuY3JlYXRlT3ZlcmxheSh0aGlzLmhlbHBUb29sdGlwRWxlbWVudCwgWzE1LCAwXSwgJ2NlbnRlci1sZWZ0Jyk7XG4gICAgcmV0dXJuIHRoaXMucmVuZGVyZWRNYXAuYWRkT3ZlcmxheSh0aGlzLmhlbHBUb29sdGlwKTtcbiAgfSxcbiAgY3JlYXRlTWVhc3VyZVRvb2x0aXA6IGZ1bmN0aW9uKCkge1xuICAgIGlmICh0aGlzLm1lYXN1cmVUb29sdGlwRWxlbWVudCkge1xuICAgICAgdGhpcy5tZWFzdXJlVG9vbHRpcEVsZW1lbnQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZCh0aGlzLm1lYXN1cmVUb29sdGlwRWxlbWVudCk7XG4gICAgfVxuICAgIHRoaXMubWVhc3VyZVRvb2x0aXBFbGVtZW50ID0gdGhpcy5jcmVhdGVEaXZFbGVtZW50KCd0b29sdGlwIHRvb2x0aXAtbWVhc3VyZScpO1xuICAgIHRoaXMubWVhc3VyZVRvb2x0aXAgPSB0aGlzLmNyZWF0ZU92ZXJsYXkodGhpcy5tZWFzdXJlVG9vbHRpcEVsZW1lbnQsIFswLCAtMTVdLCAnYm90dG9tLWNlbnRlcicpO1xuICAgIHJldHVybiB0aGlzLnJlbmRlcmVkTWFwLmFkZE92ZXJsYXkodGhpcy5tZWFzdXJlVG9vbHRpcCk7XG4gIH0sXG4gIGNyZWF0ZURpdkVsZW1lbnQ6IGZ1bmN0aW9uKGNsYXNzTmFtZSkge1xuICAgIHZhciBlbGVtZW50O1xuICAgIGVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICBlbGVtZW50LmNsYXNzTmFtZSA9IGNsYXNzTmFtZTtcbiAgICByZXR1cm4gZWxlbWVudDtcbiAgfSxcbiAgY3JlYXRlT3ZlcmxheTogZnVuY3Rpb24oZWxlbWVudCwgb2Zmc2V0LCBwb3NpdGlvbikge1xuICAgIHJldHVybiBuZXcgT2wzLk92ZXJsYXkoe1xuICAgICAgZWxlbWVudDogZWxlbWVudCxcbiAgICAgIG9mZnNldDogb2Zmc2V0LFxuICAgICAgcG9zaXRpb25pbmc6IHBvc2l0aW9uXG4gICAgfSk7XG4gIH0sXG4gIGZvcm1hdExlbmd0aExpbmVTdHJpbmc6IGZ1bmN0aW9uKGxpbmUpIHtcbiAgICB2YXIgY29vcmRpbmF0ZXMsIGluZGV4LCBsZW4sIGxlbmd0aCwgc291cmNlUHJvajtcbiAgICBjb29yZGluYXRlcyA9IGxpbmUuZ2V0Q29vcmRpbmF0ZXMoKTtcbiAgICBzb3VyY2VQcm9qID0gdGhpcy5yZW5kZXJlZE1hcC5nZXRWaWV3KCkuZ2V0UHJvamVjdGlvbigpO1xuICAgIGxlbmd0aCA9IDA7XG4gICAgaW5kZXggPSAwO1xuICAgIGxlbiA9IGNvb3JkaW5hdGVzLmxlbmd0aCAtIDE7XG4gICAgd2hpbGUgKGluZGV4IDwgbGVuKSB7XG4gICAgICBsZW5ndGggKz0gdGhpcy5mb3JtYXRMZW5ndGhUcmFuc2Zvcm0oY29vcmRpbmF0ZXNbaW5kZXhdLCBjb29yZGluYXRlc1tpbmRleCArIDFdKTtcbiAgICAgICsraW5kZXg7XG4gICAgfVxuICAgIHJldHVybiB0aGlzLmZvcm1hdENvbXBsZXRlTGVuZ3RoKGxlbmd0aCk7XG4gIH0sXG4gIGZvcm1hdExlbmd0aFRyYW5zZm9ybTogZnVuY3Rpb24oY29vcmQxLCBjb29yZDIpIHtcbiAgICB2YXIgYzEsIGMyLCBzb3VyY2VQcm9qLCB3Z3M4NFNwaGVyZTtcbiAgICB3Z3M4NFNwaGVyZSA9IG5ldyBPbDMuU3BoZXJlKENvbnN0YW50cy5XR1M4NFNQSEVSRSk7XG4gICAgc291cmNlUHJvaiA9IHRoaXMucmVuZGVyZWRNYXAuZ2V0VmlldygpLmdldFByb2plY3Rpb24oKTtcbiAgICBjMSA9IE9sMy5wcm9qLnRyYW5zZm9ybShjb29yZDEsIHNvdXJjZVByb2osICdFUFNHOjQzMjYnKTtcbiAgICBjMiA9IE9sMy5wcm9qLnRyYW5zZm9ybShjb29yZDIsIHNvdXJjZVByb2osICdFUFNHOjQzMjYnKTtcbiAgICByZXR1cm4gd2dzODRTcGhlcmUuaGF2ZXJzaW5lRGlzdGFuY2UoYzEsIGMyKTtcbiAgfSxcbiAgZm9ybWF0Q29tcGxldGVMZW5ndGg6IGZ1bmN0aW9uKGxlbmd0aCkge1xuICAgIHZhciBmb3JtYXRlZExlbmd0aDtcbiAgICBpZiAobGVuZ3RoID4gMTAwKSB7XG4gICAgICBmb3JtYXRlZExlbmd0aCA9IE1hdGgucm91bmQobGVuZ3RoIC8gMTAwMCAqIDEwMCkgLyAxMDAgKyAnICcgKyAna20nO1xuICAgIH0gZWxzZSB7XG4gICAgICBmb3JtYXRlZExlbmd0aCA9IE1hdGgucm91bmQobGVuZ3RoICogMTAwKSAvIDEwMCArICcgJyArICdtJztcbiAgICB9XG4gICAgcmV0dXJuIGZvcm1hdGVkTGVuZ3RoO1xuICB9XG59O1xuXG5pZiAodHlwZW9mIG1vZHVsZSAhPT0gXCJ1bmRlZmluZWRcIiAmJiBtb2R1bGUgIT09IG51bGwpIHtcbiAgbW9kdWxlLmV4cG9ydHMgPSBtZWFzdXJlbWVudFRvb2w7XG59XG4iLCJ2YXIgUmVhY3QsIGFwcE1hcFBhbmVsO1xuXG5SZWFjdCA9ICh0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93WydSZWFjdCddIDogdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbFsnUmVhY3QnXSA6IG51bGwpO1xuXG5hcHBNYXBQYW5lbCA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtcbiAgZGlzcGxheU5hbWU6ICdQYW5lbENvbXBvbmVudCcsXG4gIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGxlbmd0aHM6IFtdLFxuICAgICAgc2hvd0xlbmd0aHM6IGZhbHNlLFxuICAgICAgcmVzdWx0TGVuZ3RoOiAnJ1xuICAgIH07XG4gIH0sXG4gIGNyZWF0ZVJvd3M6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiB0aGlzLnN0YXRlLmxlbmd0aHMubWFwKGZ1bmN0aW9uKGl0ZW0sIGkpIHtcbiAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtcbiAgICAgICAgXCJjbGFzc05hbWVcIjogJ3JvdydcbiAgICAgIH0sIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIG51bGwsIGkgKyAxLCBcIi5cIiwgaXRlbSkpO1xuICAgIH0pO1xuICB9LFxuICBjcmVhdGVIZWFkZXJSb3c6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtcbiAgICAgIFwiY2xhc3NOYW1lXCI6ICdyb3ctaGVhZGVyJ1xuICAgIH0sIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIG51bGwsIFwiw5pzZWtcIiksIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIG51bGwsIFwiVnpkw6FsZW5vc3RcIikpO1xuICB9LFxuICBjcmVhdGVSZXN1bHRSb3c6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtcbiAgICAgIFwiY2xhc3NOYW1lXCI6ICdyZXN1bHRMZW5ndGgnXG4gICAgfSwgUmVhY3QuY3JlYXRlRWxlbWVudChcInNwYW5cIiwgbnVsbCwgXCJDZWxrb3bDoSBkw6lsa2EgdHJhc3lcIiksIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwXCIsIG51bGwsIFwiIFwiLCBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3Ryb25nXCIsIG51bGwsIHRoaXMuc3RhdGUucmVzdWx0TGVuZ3RoKSwgXCIgXCIpKTtcbiAgfSxcbiAgY3JlYXRlTm90aWNlOiBmdW5jdGlvbigpIHtcbiAgICB2YXIgc3R5bGU7XG4gICAgc3R5bGUgPSB7XG4gICAgICBjb2xvcjogJyNjY2MnLFxuICAgICAgZm9udFNpemU6ICcyMnB4JyxcbiAgICAgIHRleHRBbGlnbjogJ2NlbnRlcidcbiAgICB9O1xuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCB7XG4gICAgICBcInN0eWxlXCI6IHN0eWxlXG4gICAgfSwgXCJaYWRlanRlIGFsZXNwb8WIIGR2YSBib2R5XCIpO1xuICB9LFxuICByZW5kZXI6IGZ1bmN0aW9uKCkge1xuICAgIHZhciBoZWFkZXJSb3dzLCBub3RpY2UsIHJlc3VsdExlbmd0aCwgcm93c09mTGVuZ3RocztcbiAgICByb3dzT2ZMZW5ndGhzID0gdGhpcy5jcmVhdGVSb3dzKCk7XG4gICAgaGVhZGVyUm93cyA9IHRoaXMuc3RhdGUubGVuZ3Rocy5sZW5ndGggIT09IDAgPyB0aGlzLmNyZWF0ZUhlYWRlclJvdygpIDogdm9pZCAwO1xuICAgIHJlc3VsdExlbmd0aCA9IHRoaXMuc3RhdGUucmVzdWx0TGVuZ3RoID8gdGhpcy5jcmVhdGVSZXN1bHRSb3coKSA6IHZvaWQgMDtcbiAgICBub3RpY2UgPSB0aGlzLnN0YXRlLmxlbmd0aHMubGVuZ3RoID09PSAwID8gdGhpcy5jcmVhdGVOb3RpY2UoKSA6IG51bGw7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwgbnVsbCwgUmVhY3QuY3JlYXRlRWxlbWVudChcImgyXCIsIG51bGwsIFwiTcSbxZllbsOtIGTDqWxla1wiKSwgbm90aWNlLCBoZWFkZXJSb3dzLCByb3dzT2ZMZW5ndGhzLCByZXN1bHRMZW5ndGgpO1xuICB9XG59KTtcblxuaWYgKHR5cGVvZiBtb2R1bGUgIT09IFwidW5kZWZpbmVkXCIgJiYgbW9kdWxlICE9PSBudWxsKSB7XG4gIG1vZHVsZS5leHBvcnRzID0gYXBwTWFwUGFuZWw7XG59XG4iLCJ2YXIgQ29uc3RhbnRzLCBNZWFzdXJlbWVudFRvb2wsIE9sMywgdmVjdG9yTGF5ZXI7XG5cbk9sMyA9ICh0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93WydvbCddIDogdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbFsnb2wnXSA6IG51bGwpO1xuXG5NZWFzdXJlbWVudFRvb2wgPSByZXF1aXJlKCcuL21lYXN1cmVtZW50VG9vbC5qcycpO1xuXG5Db25zdGFudHMgPSByZXF1aXJlKCcuL2NvbnN0YW50cy5qcycpO1xuXG52ZWN0b3JMYXllciA9IHtcblxuICAvKipcbiAgICAgKiBSZW5kZXJlZCB2ZWN0b3IgbGF5ZXJcbiAgICAgKiBAdHlwZSB7b2wubGF5ZXIuVmVjdG9yfVxuICAgKi9cbiAgbGF5ZXI6IG51bGwsXG5cbiAgLyoqXG4gICAgICogUmVuZGVyZWQgbWFwXG4gICAgICogQHR5cGUge29sLk1hcH1cbiAgICovXG4gIHJlbmRlcmVkTWFwOiBudWxsLFxuXG4gIC8qXG4gICAgKiBJbml0aWFsaXphdGlvbiBvZiB0aGUgbGF5ZXJcbiAgICAqXG4gICAgKiBAcGFyYW0ge29sLk1hcH0gcmVuZGVyZWRNYXAgLSByZW5kZXJlZCBvbDMgbWFwXG4gICAgKiBAcmV0dXJuIHtvbC5sYXllci5WZWN0b3J9XG4gICAqL1xuICBjcmVhdGVMYXllcjogZnVuY3Rpb24ocmVuZGVyZWRNYXApIHtcbiAgICB0aGlzLnJlbmRlcmVkTWFwID0gcmVuZGVyZWRNYXA7XG4gICAgdGhpcy5sYXllciA9IG5ldyBPbDMubGF5ZXIuVmVjdG9yKHtcbiAgICAgIHRpdGxlOiAnVmVjdG9yJyxcbiAgICAgIHNvdXJjZTogbmV3IE9sMy5zb3VyY2UuVmVjdG9yKCksXG4gICAgICBzdHlsZTogdGhpcy5jcmVhdGVTdHlsZXMoKVxuICAgIH0pO1xuICAgIE1lYXN1cmVtZW50VG9vbC5pbml0KHRoaXMucmVuZGVyZWRNYXAsIHRoaXMubGF5ZXIpO1xuICAgIHJldHVybiB0aGlzLmxheWVyO1xuICB9LFxuXG4gIC8qXG4gICAgKiBDcmVhdGUgc3R5bGVzIG9mIHZlY3RvciBsYXllclxuICAgICpcbiAgICAqIEByZXR1cm4ge0FycmF5fVxuICAgKi9cbiAgY3JlYXRlU3R5bGVzOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gW1xuICAgICAgbmV3IE9sMy5zdHlsZS5TdHlsZSh7XG4gICAgICAgIGZpbGw6IG5ldyBPbDMuc3R5bGUuRmlsbCh7XG4gICAgICAgICAgY29sb3I6IENvbnN0YW50cy5zdHlsZS52ZWN0b3JMYXllci5GSUxMQ09MT1JcbiAgICAgICAgfSksXG4gICAgICAgIHN0cm9rZTogbmV3IE9sMy5zdHlsZS5TdHJva2Uoe1xuICAgICAgICAgIGNvbG9yOiBDb25zdGFudHMuc3R5bGUudmVjdG9yTGF5ZXIuU1RST0tFQ09MT1IsXG4gICAgICAgICAgd2lkdGg6IENvbnN0YW50cy5zdHlsZS52ZWN0b3JMYXllci5XSURUSFxuICAgICAgICB9KSxcbiAgICAgICAgaW1hZ2U6IG5ldyBPbDMuc3R5bGUuQ2lyY2xlKHtcbiAgICAgICAgICByYWRpdXM6IENvbnN0YW50cy5zdHlsZS52ZWN0b3JMYXllci5SQURJVVMsXG4gICAgICAgICAgZmlsbDogbmV3IE9sMy5zdHlsZS5GaWxsKHtcbiAgICAgICAgICAgIGNvbG9yOiBDb25zdGFudHMuc3R5bGUudmVjdG9yTGF5ZXIuRklMTENPTE9SXG4gICAgICAgICAgfSlcbiAgICAgICAgfSlcbiAgICAgIH0pLCBuZXcgT2wzLnN0eWxlLlN0eWxlKHtcbiAgICAgICAgc3Ryb2tlOiBuZXcgT2wzLnN0eWxlLlN0cm9rZSh7XG4gICAgICAgICAgY29sb3I6IENvbnN0YW50cy5zdHlsZS52ZWN0b3JMYXllci5TVFJPS0VDT0xPUlNFQ09ORCxcbiAgICAgICAgICB3aWR0aDogQ29uc3RhbnRzLnN0eWxlLnZlY3RvckxheWVyLldJRFRIU0VDT05EXG4gICAgICAgIH0pXG4gICAgICB9KVxuICAgIF07XG4gIH1cbn07XG5cbmlmICh0eXBlb2YgbW9kdWxlICE9PSBcInVuZGVmaW5lZFwiICYmIG1vZHVsZSAhPT0gbnVsbCkge1xuICBtb2R1bGUuZXhwb3J0cyA9IHZlY3RvckxheWVyO1xufVxuIl19
